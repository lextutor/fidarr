﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace FidarrAuthentication.Abstractions
{
    public interface IJWTService<TEntity> where TEntity : IdentityUser
    {
        Task<string> GetToken(TEntity entity);
    }
}
