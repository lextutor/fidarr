﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.IdentityModel.Tokens;
using FidarrAuthentication.Abstractions;
using FidarrAuthentication.Implementation;

namespace FidarrAuthentication.Extensions
{
    public static class JWTConfiguration
    {
            public static void UseJWTAuthentication(this IServiceCollection services, IConfiguration config)
            {
                services.AddAuthentication(options =>
                {
                    options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                    options.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
                    options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                })
                .AddJwtBearer(options =>
                {
                    options.RequireHttpsMetadata = false;
                    options.SaveToken = true;
                    options.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateIssuer = true,
                        ValidateLifetime = true,
                        ValidateAudience = true,
                        ValidateIssuerSigningKey = true,
                        ValidIssuer = config["JWTConfigurations:Issuer"],
                        ValidAudience = config["JWTConfigurations:Audience"],
                        IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(config["JWTConfigurations:SecretKey"])),
                        ClockSkew = TimeSpan.Zero
                    };
                });
            }


            public static void UseAuthorization(this IServiceCollection services)
            {
                services.AddAuthorization(configure =>
                {
                    configure.AddPolicy(Policies.Policies.Administration, Policies.Policies.AdministrationPolicy());
                    configure.AddPolicy(Policies.Policies.Master, Policies.Policies.MasterPolicy());
                    configure.AddPolicy(Policies.Policies.ContentManager, Policies.Policies.ContentManagerPolicy());
                });
            }

            public static void UseJwtConfig(this IServiceCollection services)
            {
                services.AddScoped(typeof(IJWTService<>), typeof(JWTService<>));
            }
    }
}
