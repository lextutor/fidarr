﻿using FidarrAuthentication.Abstractions;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace FidarrAuthentication.Implementation
{
    public class JWTService<TEntity> : IJWTService<TEntity> where TEntity : IdentityUser
    {
        private readonly IConfiguration _config;
        private readonly UserManager<TEntity> _userManager;

        public JWTService(IServiceProvider serviceProvider)
        {
            _config = serviceProvider.GetRequiredService<IConfiguration>();
            _userManager = serviceProvider.GetRequiredService<UserManager<TEntity>>();
        }
        public async Task<string> GetToken(TEntity  Entity)
        {
            var authClaims = new List<Claim>
            {
                new Claim(ClaimTypes.NameIdentifier, Entity.Id),
                new Claim(ClaimTypes.Name, Entity.UserName),
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString())
            };

            var userRoles = await _userManager.GetRolesAsync(Entity);

            foreach (var role in userRoles)
            {
                authClaims.Add(new Claim(ClaimTypes.Role, role));
            }

            var authSignInKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_config["JWTConfigurations:SecretKey"]));
            var token = new JwtSecurityToken(
                issuer: _config["JWTConfigurations:Issuer"],
                audience: _config["JWTConfigurations:Audience"],
                expires: DateTime.Now.AddDays(1),
                claims: authClaims,
                signingCredentials: new SigningCredentials(authSignInKey, SecurityAlgorithms.HmacSha256));

            return new JwtSecurityTokenHandler().WriteToken(token);
        }
    }
}
