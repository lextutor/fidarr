﻿using Microsoft.AspNetCore.Authorization;
using System;
using System.Collections.Generic;
using System.Text;

namespace FidarrAuthentication.Policies
{
    public static class Policies
    {
        /// <summary>
        /// Admin role for our policy
        /// </summary>
        public const string Master = "Master";
        /// <summary>
        /// Decadev role for our policy
        /// </summary>
        public const string Administration = "Administration";
        /// <summary>
        /// Vendor role for our policy
        /// </summary>
        public const string ContentManager = "ContentManager";

        /// <summary>
        /// Grants Parents User rights
        /// </summary>
        /// <returns></returns>
        public static AuthorizationPolicy MasterPolicy()
        {
            return new AuthorizationPolicyBuilder().RequireAuthenticatedUser().RequireRole(Master).Build();
        }

        /// <summary>
        ///Grants guardians User rights
        /// </summary>
        /// <returns></returns>
        public static AuthorizationPolicy AdministrationPolicy()
        {
            return new AuthorizationPolicyBuilder().RequireAuthenticatedUser().RequireRole(Administration).Build();
        }

        /// <summary>
        /// Grants children User rights
        /// </summary>
        /// <returns></returns>
        public static AuthorizationPolicy ContentManagerPolicy()
        {
            return new AuthorizationPolicyBuilder().RequireAuthenticatedUser().RequireRole(ContentManager).Build();
        }

    }
}
